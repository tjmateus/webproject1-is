package test;

import base.BaseSenderManager;


public class MainSender
{

    public static void main(String[] args) throws Exception
    {
        new BaseSenderManager().startService(true);
    }

}

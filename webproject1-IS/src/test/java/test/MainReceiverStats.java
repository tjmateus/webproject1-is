package test;

import java.io.IOException;

import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import base.BaseReceiverStatsManager;


public class MainReceiverStats
{
    
    public static void main(String[] args) throws XPathExpressionException, NamingException, JMSException, IOException, ParserConfigurationException, SAXException
    {
        new BaseReceiverStatsManager("stats").init();
    }

}

package test;

import java.io.IOException;

import javax.jms.JMSException;
import javax.naming.NamingException;

import base.BaseReceiverHtmlManager;


public class MainReceiverHtml
{

    public static void main(String[] args) throws NamingException, JMSException, IOException
    {
        new BaseReceiverHtmlManager ("summary").init();

    }

}

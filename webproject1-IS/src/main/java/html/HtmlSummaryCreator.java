

package html;

import java.io.FileOutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;


public class HtmlSummaryCreator
{
    
    private final String pathToXml = "src/main/resources/xml/";
    private final String pathToHtml = "WebContent/index.html";
    private final String clientId;
    
    public HtmlSummaryCreator (String clientId) {
        this.clientId=clientId;
    }
    
    public void htmlCreator () {
        try
        {
            
            //with xml and xslt creates a html
            TransformerFactory tFactory = TransformerFactory.newInstance();

            Transformer transformer = tFactory.newTransformer(new javax.xml.transform.stream.StreamSource(pathToXml + "cnn.xsl"));
            
            transformer.transform(new javax.xml.transform.stream.StreamSource(pathToXml + clientId + ".xml"), new javax.xml.transform.stream.StreamResult(new FileOutputStream(pathToHtml)));
            
            System.out.println("Generated HTML with success");
            
        }
        catch (Exception e)
        {
            
            System.out.println("Error converting to html");
            e.printStackTrace();
        }
        
    }
    
    public String getPathToXml()
    {
        return pathToXml;
    }

}

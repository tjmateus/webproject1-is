

package jms;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class Sender
{

    private ConnectionFactory cf;
    private Connection c;
    private Session s;
    private Topic d;

    public Sender()
    {

        try
        {
            InitialContext init = new InitialContext();
            this.cf = (ConnectionFactory) init.lookup("jms/RemoteConnectionFactory");
            this.d = (Topic) init.lookup("jms/topic/test");
            this.c = (Connection) this.cf.createConnection("cnn", "cnn");
            this.c.start();
            this.s = this.c.createSession(false, Session.AUTO_ACKNOWLEDGE);
        }
        catch (JMSException | NamingException e)
        {
            System.out.println("Connection to server failed");
        }
    }

    public void send(String xml)
    {

        try
        {

            byte[] bString = xml.getBytes();

            MessageProducer mp = s.createProducer(d);
            BytesMessage bm = s.createBytesMessage();

            bm.writeBytes(bString);

            mp.send(bm);

            System.out.println("Sended bytes message!");

        }
        catch (JMSException e)
        {
            e.printStackTrace();
        }
    }

    public void close()
    {
        try{
            this.c.close();
        }catch(JMSException e){
            e.printStackTrace();
        }
    }
    
    
    public Connection getConnection()
    {
        return c;
    }

    
    public void setC(Connection c)
    {
        this.c = c;
    }

}


package jms;

import java.io.IOException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public abstract class Receiver implements MessageListener
{

    private String PATH = "src/main/resources/xml/";
    
    private ConnectionFactory cf;
    private Connection c;
    private Session s;
    private Topic topic;
    private MessageConsumer mc;
    
    private String clientId;

    public Receiver(String clientId)
    {
        this.clientId=clientId;
    }
    
    public void init () throws NamingException, JMSException, IOException {
        InitialContext init = new InitialContext();
        this.cf = (ConnectionFactory) init.lookup("jms/RemoteConnectionFactory");
        this.topic = (Topic) init.lookup("jms/topic/test");
        this.c = (Connection) this.cf.createConnection("cnn", "cnn");
        this.c.setClientID(clientId);
        this.c.start();
        this.s = this.c.createSession(false, Session.AUTO_ACKNOWLEDGE);
        mc = s.createDurableSubscriber(topic, "cnn");
        mc.setMessageListener(this);
        PATH = PATH + clientId + ".xml";
        System.out.println("Client with id " + clientId + " connected");
        System.in.read();
    }
    
    public String getClientId () {
        return this.clientId;
    }
    
    public void close() throws JMSException
    {
        this.c.close();
    }
    
    public String getPATH()
    {
        return PATH;
    }
  

}
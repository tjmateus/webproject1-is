

package crawler;

import jms.Sender;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import storage.pgsql.PgsqlStorageService;


public class RetrySend implements Job
{

    public void execute(JobExecutionContext context) throws JobExecutionException
    {

        PgsqlStorageService pgsql = (PgsqlStorageService) context.getMergedJobDataMap().get("connection");

        int count = pgsql.countUnpublishedNews();
        
        
        if (count > 0)
        {

            System.out.println("Retrying send " + count + " messages...");
            
            try
            {
                Sender sender = new Sender();

                if (sender.getConnection() != null)
                {

                    int i = 1;
                    //check if there are unpublished messages
                    for (String xml : pgsql.getNews("unpublished"))
                    {

                        System.out.println("Retrying send message " + i++ + " ...");
                        sender.send(xml);
                        //if sent delete unpublished and insert into published
                        pgsql.deleteNews();
                        pgsql.insertNews("published", xml);

                    }
                    sender.close();

                }
                else
                {
                    System.out.println("Fail sending message.");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        
        else {
            
            System.out.println("No messages unpublished...");
            
        }

    }

}

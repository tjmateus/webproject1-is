

package crawler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.AllNews;
import model.Authors;
import model.Highlights;
import model.News;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import storage.file.FileNews;


public class WebCrawler
{

    private String URL = "http://www.edition.cnn.com";
    private AllNews allnews = new AllNews();
    List<String> allUrls = new ArrayList<>();
    private int id = 0;

    public void processLinks() throws SQLException, IOException, InterruptedException
    {

        try
        {
            Document doc = Jsoup.connect(URL).get();

            Element element = doc.getElementById("intl-menu");

            Elements links = element.select("a");

            for (Element link : links)
            {
                if (link.attr("href").startsWith("http"))
                {
                    continue;
                }
                else if (link.attr("href").equals("/"))
                {
                    continue;
                }
                else
                {
                    if (!link.attr("href").equals("/video/"))
                    {
                        processTab(URL + link.attr("href"));
                    }
                }
            }


            FileNews file = new FileNews();
            file.write(allnews);
        }

        catch (Exception e)
        {
           System.out.println("Catch Exception processing links");
           Thread.sleep(5000);
           processLinks();
        }

    }

    private void processTab(String link) throws IOException, InterruptedException, Exception
    {

        try
        {
            Document doc = Jsoup.connect(link).get();

            List<String> urls = new ArrayList<>();

            Element mainContainer = doc.getElementById("cnn_maincntnr");

            Element editorChoice = mainContainer.getElementById("cnn_maintt2bul");
            Elements links = editorChoice.getAllElements();
            for (Element e : links)
            {
                String url;
                if (!(url = e.attr("href")).equals("") && !urls.contains(url))
                {
                    urls.add(url);
                }
            }

            Element mainTop = mainContainer.getElementById("cnn_maintoplive");
            links = mainTop.getAllElements();
            for (Element e : links)
            {
                String url;
                if (!(url = e.attr("href")).equals("") && !urls.contains(url))
                {
                    urls.add(url);
                }
            }

            Element isisAnalysis = mainContainer.getElementById("cnn_maintopprofile");
            links = isisAnalysis.getAllElements();
            for (Element e : links)
            {
                String url;
                if (!(url = e.attr("href")).equals("") && !urls.contains(url))
                {
                    urls.add(url);
                }
            }

            Element aroundTheWorld = mainContainer.getElementById("cnn_fabcontent");
            links = aroundTheWorld.getAllElements();
            for (Element e : links)
            {
                String url;
                if (!(url = e.attr("href")).equals("") && !urls.contains(url))
                {
                    urls.add(url);
                }
            }

            for (String e : urls)
            {
                if (e.startsWith("/201"))
                {
                    if (!e.contains("/gallery/"))
                    {
                        processNewsPage(e);
                    }
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Catch Exception processing Tab " + link);
            Thread.sleep(5000);
            processTab(link);
        }
    }

    private void processNewsPage(String link) throws IOException
    {

        try
        {

            if (!allUrls.contains(URL + link))
            {
                allUrls.add(URL + link);

                String title = null;
                String url = null;
                List<String> highlights = new ArrayList<>();
                String date = null;
                String author = null;
                String textNews = null;
                String urlPhoto = null;
                String urlVideo = null;
                String type = null;

                News news = new News();
                Authors authors = new Authors();
                Highlights high = new Highlights();

                type = link.substring(12);
                type = type.substring(0, type.indexOf("/"));

                link = URL + link;

                Document doc = Jsoup.connect(link).get();

                Elements content = doc.getAllElements();
                content = content.select("meta");
                for (Element e : content)
                {
                    if (e.attr("name").equals("pubdate"))
                    {
                        date = e.attr("content");
                    }
                }

                content = doc.getElementById("cnnContentContainer").getAllElements();

                title = content.select("h1").text();
                url = link;

                for (Element e : content)
                {

                    if (e.attr("class").equals("cnn_bulletbin cnnStryHghLght"))
                    {
                        int size = e.getElementsByTag("li").size();
                        for (int i = 0; i < size; i++)
                        {
                            highlights.add(e.getElementsByTag("li").get(i).text());
                            high.getHighlight().add(e.getElementsByTag("li").get(i).text());
                        }
                    }

                    if (e.attr("class").equals("cnnByline"))
                    {
                        author = e.getElementsByTag("strong").text();

                        for (String s : author.split(","))
                        {
                            authors.getAuthor().add(s);
                        }

                    }

                    if (e.attr("class").equals("cnn_strycntntlft"))
                    {
                        textNews = e.getElementsByTag("p").text();
                    }

                    if (e.attr("class").equals("cnn_stryimg640captioned"))
                    {
                        urlPhoto = e.select("img").first().absUrl("src");
                    }

                    if (e.attr("class").equals("cnnArticleGalleryPhotoContainer"))
                    {
                        urlPhoto = e.select("img").first().absUrl("src");
                    }

                    if (e.attr("class").equals("OUTBRAIN"))
                    {
                        urlVideo = e.attr("data-src");
                    }

                }

                news.setId("n" + id++);
                news.setTitle(title);
                news.setUrl(url);
                news.setDate(date);
                news.setText(textNews);
                news.setPhoto(urlPhoto);
                news.setVideo(urlVideo);
                news.setAuthors(authors);
                news.setHighlights(high);
                news.setType(type);

                if (news.getAuthors().getAuthor().size() != 0 && news.getHighlights().getHighlight().size() != 0)
                {

                    allnews.getNews().add(news);

                }


                System.out.println("Added: n" + id + " with title " + title);

            }

        }
        catch (Exception e)
        {
            System.out.println("WARNING: " + e.getMessage());
        }

    }


    public AllNews getAllnews()
    {
        return allnews;
    }


}
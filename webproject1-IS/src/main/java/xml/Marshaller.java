package xml;

import java.util.List;

import model.News;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class Marshaller
{
    //parse of classes java to xml
    public String marshal(List <News> allNews) {
        
        // XML DATASOURCE
        Element allNewsXml = new Element("all-news");
        allNewsXml.setAttribute(new Attribute("timestamp", "123456789"));
        allNewsXml.setAttribute(new Attribute("timezone", "GMT"));
        allNewsXml.setAttribute(new Attribute("version", "1.0"));
        
        Document documentXML = new Document(allNewsXml);
        
        for (News news : allNews) {
            Element newsXml = new Element ("news");
            newsXml.setAttribute(new Attribute("id", news.getId()));
            
            Element titleXml = new Element ("title");
            titleXml.setText(news.getTitle());
            newsXml.addContent(titleXml);
            
            Element urlXml = new Element ("url");  
            urlXml.setText(news.getUrl());
            newsXml.addContent(urlXml);
            
            Element highlightsXml = new Element ("highlights");
            for (String highlight : news.getHighlights().getHighlight()){
                Element highlightXml = new Element ("highlight");
                highlightXml.setText(highlight);
                highlightsXml.addContent(highlightXml);
            }
            newsXml.addContent(highlightsXml);
            
            Element authorsXml = new Element ("authors");
            for (String author : news.getAuthors().getAuthor()){
                Element authorXml = new Element ("author");
                authorXml.setText(author);
                authorsXml.addContent(authorXml);
            }
            newsXml.addContent(authorsXml);
            
            Element dateXml = new Element ("date");
            dateXml.setText(news.getDate());
            newsXml.addContent(dateXml);
            
            Element textXml = new Element ("text");
            textXml.setText(news.getText());
            newsXml.addContent(textXml);
            
            Element photoXml = new Element ("photo");
            photoXml.setText(news.getPhoto());
            newsXml.addContent(photoXml);
            
            Element videoXml = new Element ("video");
            videoXml.setText(news.getVideo());
            newsXml.addContent(videoXml);

            Element typeXml = new Element ("type");
            typeXml.setText(news.getType());
            newsXml.addContent(typeXml);
            
            allNewsXml.addContent(newsXml);
        }
        
        // XML OUTPUT
        XMLOutputter xmlOutputter = new XMLOutputter();
        xmlOutputter.setFormat(Format.getPrettyFormat());
        return xmlOutputter.outputString(documentXML);
        
    }
    
}

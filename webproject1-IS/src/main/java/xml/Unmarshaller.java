package xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import model.Authors;
import model.Highlights;
import model.News;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


public class Unmarshaller
{

    //get from a xml all news
    public List <News> unmarshal (String xml) throws JDOMException, IOException {
        
        List <News> newsList = new ArrayList <> ();
        
        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<all-news timestamp=\"1413160591704\" timezone=\"GMT\" version=\"1.0\">" 
                + xml + "</all-news>";
        
        xml = xml.replaceAll("\\\\\"", "\"");
        
        // XMLReaderJDOMFactory factory = new XMLReaderXSDFactory(getClass().getResource("/schema.xsd"));
        SAXBuilder parser = new SAXBuilder();
        
        InputStream stream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
        Document document = parser.build(stream);

        Element rootElement = document.getRootElement();
        
        List <Element> allNews = rootElement.getChildren();
        
        for (Element news : allNews) {
            
            News newsObj = new News();
            
            Authors authors = new Authors ();
            Highlights highlights = new Highlights ();
            
            Element authorsElement = news.getChild("authors");
            
            List <Element> authorsElements = authorsElement.getChildren();
            
            for (Element aElement : authorsElements) {
                authors.getAuthor().add(aElement.getValue());
            }
            
            Element highlightsElement = news.getChild("highlights");
            
            List <Element> highlightsElements = highlightsElement.getChildren();
            
            for (Element hElement : highlightsElements) {
                highlights.getHighlight().add(hElement.getValue());
            }
            
            newsObj.setId(news.getAttribute("id").getValue());
            newsObj.setTitle(news.getChild("title").getValue());
            newsObj.setUrl(news.getChild("url").getValue());
            newsObj.setDate(news.getChild("date").getValue());
            newsObj.setText(news.getChild("text").getValue());
            newsObj.setPhoto(news.getChild("photo").getValue());
            newsObj.setVideo(news.getChild("video").getValue());
            newsObj.setAuthors(authors);
            newsObj.setHighlights(highlights);
            newsObj.setType(news.getChild("type").getValue());
            
            newsList.add(newsObj);
        }
        
        return newsList;
        
    }
    
}

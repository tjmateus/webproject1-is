package model;

import java.util.Date;

public class Stats
{

    private int number;
    private Date startDate;
    private Date endDate;
    
    public Stats (int number, Date startDate, Date endDate) {
        this.number=number;
        this.startDate=startDate;
        this.endDate=endDate;
    }

    
    public int getNumber()
    {
        return number;
    }

    
    public void setNumber(int number)
    {
        this.number = number;
    }

    
    public Date getStartDate()
    {
        return startDate;
    }

    
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    
    public Date getEndDate()
    {
        return endDate;
    }

    
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }
    
}

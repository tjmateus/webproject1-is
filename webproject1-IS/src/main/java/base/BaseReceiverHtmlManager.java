

package base;

import html.HtmlSummaryCreator;

import java.io.FileOutputStream;
import java.io.IOException;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.naming.NamingException;

import jms.Receiver;
import xml.Validation;


public class BaseReceiverHtmlManager extends Receiver
{

    public BaseReceiverHtmlManager(String clientId)
    {
        super(clientId);
    }

    public void startHtmlReceiver() throws NamingException, JMSException, IOException
    {

        //generate a new html receiver with that client id
        HtmlSummaryCreator html = new HtmlSummaryCreator(this.getClientId());

        //this is validating xml when received
        System.out.println("Validating XML...");
        if (new Validation().validateXMLSchema(html.getPathToXml() + this.getClientId() + ".xml") == true)
        {
            System.out.println("XML received validated with success");
            System.out.println("Creating HTML...");
            //creation of html
            html.htmlCreator();
        }

        else
        {
            System.out.println("Xml Received Not Valid");
        }

    }

    //listener of message incoming
    @Override
    public void onMessage(Message msg)
    {
        BytesMessage bmsg = (BytesMessage) msg;
        String PATH = this.getPATH();
        //convert array of bytes into file
        try
        {
            //read bytes presents on bytes message
            FileOutputStream fileOuputStream = new FileOutputStream(PATH);
            byte[] bFile = new byte[(int) bmsg.getBodyLength()];
            bmsg.readBytes(bFile);
            //write bytes to a local file
            fileOuputStream.write(bFile);
            fileOuputStream.close();
            System.out.println("Message Read and saved to file " + PATH);
            //start receiver and processment of xml
            startHtmlReceiver();
            
        }
        catch (IOException | JMSException | NamingException e)
        {
            e.printStackTrace();
        }

    }

}

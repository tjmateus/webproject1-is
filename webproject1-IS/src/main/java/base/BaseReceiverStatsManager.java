package base;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import jms.Receiver;
import model.Stats;

import org.xml.sax.SAXException;

import statistics.StatsProducer;
import storage.file.FileStats;
import storage.pgsql.PgsqlStorageService;
import xml.Validation;


public class BaseReceiverStatsManager extends Receiver
{

    public BaseReceiverStatsManager(String clientId) throws NamingException, JMSException, IOException
    {
        super(clientId);
    }

    public void startStatsReceiver () throws NamingException, JMSException, IOException, XPathExpressionException, ParserConfigurationException, SAXException, SQLException {
        
        //creation of a new stats receiver with this client id
        StatsProducer statsProducer = new StatsProducer (this.getClientId());
        
        //validation of xml received against xsd
        if (new Validation().validateXMLSchema(statsProducer.getPath()) == true){
           
            System.out.println("XML received validated with success");
            
            //producing of stats
            Stats stats = statsProducer.produceStats();
            
            System.out.println("Inserting into BD...");
            PgsqlStorageService pgsql = new PgsqlStorageService();
            pgsql.insertStats(stats.getNumber(), stats.getStartDate(), stats.getEndDate());
            System.out.println("Insertion to BD with success");
            
            System.out.println("Writing stats...");
            new FileStats ().writeFilesStats(pgsql, stats);;
            System.out.println("Files Stats written with success");
            
        }
        
        else {
            System.out.println("XML Received Not Valid");
        }
        
        
    }
    
    @Override
    public void onMessage(Message msg)
    {
        BytesMessage bmsg = (BytesMessage) msg;
        String PATH = this.getPATH(); 
        //convert array of bytes into file
        try
        {
            FileOutputStream fileOuputStream = new FileOutputStream(PATH);
            byte[] bFile = new byte[(int) bmsg.getBodyLength()];
            bmsg.readBytes(bFile);
            fileOuputStream.write(bFile);
            fileOuputStream.close();
            System.out.println("Message Read and saved to file " + PATH);
            
            startStatsReceiver();
            
        }
        catch (IOException | JMSException | XPathExpressionException | NamingException | ParserConfigurationException | SAXException e)
        {
            e.printStackTrace();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }
    
}

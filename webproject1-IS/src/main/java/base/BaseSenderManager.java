

package base;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jms.Sender;
import model.News;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import storage.file.FileNews;
import storage.pgsql.PgsqlStorageService;
import xml.Marshaller;
import crawler.RetrySend;
import crawler.WebCrawler;


public class BaseSenderManager
{

    private Scheduler sch;

    public BaseSenderManager() throws SchedulerException
    {

        SchedulerFactory schFactory = new StdSchedulerFactory();
        sch = schFactory.getScheduler();
        sch.start();

    }

    public void startService(boolean toCrawler) throws SchedulerException, SQLException, IOException, InterruptedException
    {

        PgsqlStorageService pgsql = new PgsqlStorageService();

        //quartz is a scheduler that will check if there are messages unpublished
        startQuartz(pgsql);

        List<News> allNews = new ArrayList<>();

        if (toCrawler)
        {

            //do crawler of site and receive allNews
            WebCrawler wc = new WebCrawler();
            wc.processLinks();
            allNews = wc.getAllnews().getNews();

        }

        else
        {
            //if we don't crawling, but instead read from file
            FileNews f = new FileNews();
            allNews = f.read().getNews();

        }

        if (allNews.size() != 0)
        {
            
            Marshaller marshaller = new Marshaller();
            //use of marshal to produce xml from java classes
            String xml = marshaller.marshal(allNews);

            System.out.println("Trying to connect...");
            Sender sender = new Sender();

            //send message to JMS topic
            if (sender.getConnection() != null)
            {
                sender.send(xml);
                sender.close();
                pgsql.insertNews("published", xml);
            }
            else
            {
                System.out.println("Fail sending message. Inserting into unpublished...");
                pgsql.insertNews("unpublished", xml);
            }

        }

    }

    private void startQuartz(PgsqlStorageService pgsql) throws SchedulerException
    {

        JobDataMap jobDataMap = new JobDataMap();

        jobDataMap.put("connection", pgsql);

        JobDetail job = JobBuilder.newJob(RetrySend.class).withIdentity("crawler").setJobData(jobDataMap).build();

        if (!sch.checkExists(job.getKey()))
        {

            System.out.println("Job scheduled...");
            
            Trigger trigger = TriggerBuilder.newTrigger().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(30).withRepeatCount(4)).startAt(new Date(System.currentTimeMillis() + (30 * 1000))).build();

            //creates a schedule job with that trigger
            sch.scheduleJob(job, trigger);

        }

    }

}

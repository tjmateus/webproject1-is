

package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class GetPagination
 */
@WebServlet("/GetPagination")
public class GetPagination extends HttpServlet
{

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPagination()
    {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        //output is a initial pagination
        String output = "<center>" + "<ul class=\"list-inline\">";

        int j = 1;
        int thisPage = 1;

        output += "<li><a href=\"javascript:onMoreNews(" + (j - 1) + ");\">" + "Prev </a></li>";

        for (j = 0; j < 11; j++)
        {
            if ((j + 1) == 1)
            {
                thisPage = (j + 1);
                output += "<li><b>" + thisPage + "</b></li>";
            }


            else
            {

                output += "<li><a href=\"javascript:onMoreNews(" + (j + 1) + ");\">" + (j + 1) + "</a></li>";

            }

        }

        output += "<li><a href=\"javascript:onMoreNews(" + (thisPage + 1) + ");\">" + "Next </a></li>" + "</ul>" + " </center>";

        //##################### ------------------ ##############################

        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        out.println(output);
        out.close();

    }

}

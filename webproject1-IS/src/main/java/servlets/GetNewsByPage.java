

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.News;
import storage.pgsql.PgsqlStorageService;
import xml.Unmarshaller;


/**
 * Servlet implementation class GetNewsByPage
 */
@WebServlet("/GetNewsByPage")
public class GetNewsByPage extends HttpServlet
{

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetNewsByPage()
    {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        Unmarshaller unmarshaller = new Unmarshaller(); 
        PgsqlStorageService pgsql = new PgsqlStorageService();
        
        int page = Integer.parseInt(request.getParameter("numPage"));
        
        //output of referals. that's what is preseting in all news tab and is pointing to modals, where is all news info
        String outputRefs = "";
        
        //output of modals, news info
        String outputModals = "";

        List <News> allNews = new ArrayList <> ();
        
        try
        {
            allNews = unmarshaller.unmarshal(pgsql.getNewsByPage(page));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        outputRefs += "<div class=\"row\">" + " <div class=\"col-lg-12 text-center\">" 
                + "<h2 class=\"section-heading\">All News</h2>" + "</div>" + "</div>"
                + "<div id=\"dataDiv\" class=\"row\">";
        
        for (News n : allNews) {
            
            if (n.getPhoto().isEmpty()){
                n.setPhoto("img/no-image.jpg");
            }
            
            outputRefs += "<div class=\"col-md-4 col-sm-6 portfolio-item\">" 
                    + "<div class=\"div-title\"><a href=\"#"+n.getId()+"\" class=\"portfolio-link\"" 
                    + "data-toggle=\"modal\">" + "<div class=\"portfolio-hover\">" 
                    + "<div class=\"portfolio-hover-content\">" + "<i class=\"fa fa-plus fa-3x\"></i>" 
                    + "</div> </div> "
                    + "<div class=\"div-image\"><img src=\""+n.getPhoto()+"\" class=\"img-responsive\"" 
                    + "alt=\"\"  /></div>" + "</a>" + "<div class=\"portfolio-caption\">" 
                    + "<h4>"+n.getTitle()+"</h4>" 
                    + "<p class=\"text-muted\">"+n.getDate()+"</p></div>" + "</div> </div>";
            
            outputModals += "<div class=\"portfolio-modal modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" id=\""+n.getId()+"\">"
                    + "<div class=\"modal-content\">"
                    + "<div class=\"close-modal\" data-dismiss=\"modal\">"
                    + "<div class=\"lr\">"
                    + "<div class=\"rl\"></div>"
                    + "</div>"
                    + "</div>"
                    + "<div class=\"container\">"
                    + "<div class=\"row\">"
                    + "<div class=\"col-lg-8 col-lg-offset-2\">"
                    + "<div class=\"modal-body\">"
                    + "<h2>"+n.getTitle()+"</h2>"
                    + "<p class=\"item-intro text-muted\">"+n.getUrl()+"</p>"
                    + "<ul class=\"list-inline\" text-align=\"center\">"
                    + "<li>"
                    + "<strong>Authors:</strong>"
                    + "</li>"
                    + "<p></p>";
            
                    for (String str : n.getAuthors().getAuthor()){
                        
                        outputModals += "<li>"+str+"</li>";
                        
                    }
            
                    outputModals += "</ul>"
                    + "<ul class=\"list-inline\" text-align=\"center\">"
                    + "<li>"
                    + "<strong>Highlights:</strong>"
                    + "</li>"
                    + "<p></p>";
                    
                    for (String str : n.getHighlights().getHighlight()){
                        
                        outputModals += "<li>"+str+"</li>";
                        
                    }
                    
                    outputModals += "</ul>"
                    + "<img class=\"img-responsive\" alt=\"img\" src=\""+n.getPhoto()+"\"/><p>"+n.getText()+"</p>"
                    + "<ul class=\"list-inline\">"
                    + "<li>Date: "+n.getDate()+"</li>"
                    + "<li>Type: "+n.getType()+"</li>"
                    + "<li>Video: "+n.getVideo()+"</li>"
                    + "</ul>"
                    + "<button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\"><i class=\"fa fa-times\"></i> Close</button>"
                    + "</div></div></div></div></div></div>";
            
            
        }
        
        outputRefs += "</div> </div> ";
        
        
        int size = 0;
        try
        {
            size = pgsql.getCountNews();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        if (size > 6)
        {
            outputRefs += "<center>" + "<ul class=\"list-inline\">";

            if (page == 1)
            {
                outputRefs += "<li><a>Prev</a></li>";
            }
            else
            {
                outputRefs += "<li><a href=\"javascript:onMoreNews(" + (page - 1) + ");\">Prev</a></li>";
            }

            int numPageBold = 0;

            for (int j = page-1; j < page + 10 && j < (size/6) + 1; j++)
            {
                if ((j + 1) == page)
                {
                    outputRefs += "<li><b>" + (j + 1) + "</b></li>";
                    numPageBold = j + 1;
                }
                else
                    outputRefs += "<li><a>" + (j + 1) + "</a></li>";

            }
            if (numPageBold == (size / 9) + 1)
            {
                outputRefs += "<li><a>Next</a></li>" + "</ul>" + "</center>";
            }
            else
            {
                outputRefs += "<li><a href=\"javascript:onMoreNews(" + (page + 1) + ");\">Next</a></li>" + "</ul>" + "</center>";
            }
        }

        //##################### ------------------ ##############################

        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        
        String join = outputRefs + "::!!::" + outputModals;
        out.println(join);
        out.close();
        pgsql.close();
    }

}



package storage.pgsql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Stats;


public class PgsqlStorageService
{

    private Connection connection = null;

    public PgsqlStorageService()
    {

        try
        {

            Class.forName("org.postgresql.Driver");

        }
        catch (ClassNotFoundException e)
        {

            e.printStackTrace();
            return;

        }

        try
        {
            connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/project1-is", "tmateus", "postgres");
        }
        catch (SQLException e)
        {

            System.out.println("Connection Failed!");
            e.printStackTrace();
            return;

        }

    }

    //insert xml on that table
    public void insertNews(String table, String xml)
    {

        String sql = "WITH " + "to_be_inserted (news_xml) " + "AS (VALUES (XMLPARSE(DOCUMENT ?))) " + "insert into " + table + " (news_xml) " + "SELECT news_xml " + "FROM to_be_inserted " + "WHERE NOT EXISTS " + "(SELECT 1 FROM " + table + " " + "WHERE CAST(" + table + ".news_xml AS TEXT) like CAST(to_be_inserted.news_xml AS TEXT))";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {

            preparedStatement.setObject(1, xml);
            preparedStatement.addBatch();
            preparedStatement.executeBatch();

        }

        catch (SQLException e)
        {
            System.out.println(e.getNextException());
            e.printStackTrace();
        }

    }

    //get news xml from a table
    public List<String> getNews(String table)
    {

        List<String> xmls = new ArrayList<>();

        String sql = "select news_xml, id from " + table + " order by id desc";

        try (Statement stm = connection.createStatement())
        {

            try (ResultSet resultSet = stm.executeQuery(sql))
            {

                while (resultSet.next())
                {
                    xmls.add(resultSet.getString(1));
                }

            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return xmls;

    }

    //insert stats on stats table
    public void insertStats(int number, Date startDate, Date endDate)
    {

        String sql = "WITH " + "to_be_inserted (number_news, start_date, end_date) " + "AS (VALUES (?,CAST (? AS TIMESTAMP), CAST(? AS TIMESTAMP))) " + "insert into stats (number_news, start_date, end_date) " + "SELECT number_news, start_date, end_date " + "FROM to_be_inserted " + "WHERE NOT EXISTS " + "(SELECT 1 FROM stats " + "WHERE stats.number_news = to_be_inserted.number_news " + "AND stats.start_date = to_be_inserted.start_date " + "AND stats.end_date = to_be_inserted.end_date)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {

            preparedStatement.setInt(1, number);
            preparedStatement.setTimestamp(2, new Timestamp(startDate.getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(endDate.getTime()));
            preparedStatement.addBatch();
            preparedStatement.executeBatch();

        }

        catch (SQLException e)
        {
            System.out.println(e.getNextException());
            e.printStackTrace();
        }


    }

    //get stats from db
    public List<Stats> getStats()
    {

        List<Stats> statsList = new ArrayList<>();

        String sql = "select number_news, start_date, end_date from stats";

        try (Statement stm = connection.createStatement())
        {

            try (ResultSet resultSet = stm.executeQuery(sql))
            {

                while (resultSet.next())
                {
                    int number = resultSet.getInt(1);
                    Date startDate = resultSet.getDate(2);
                    Date endDate = resultSet.getDate(3);
                    Stats stats = new Stats(number, startDate, endDate);
                    statsList.add(stats);
                }

            }

        }
        catch (SQLException e)
        {
            System.out.println(e.getNextException());
            e.printStackTrace();
        }

        return statsList;

    }

    //get number of unpublished messages
    public int countUnpublishedNews()
    {

        String sql = "SELECT COUNT(*) FROM UNPUBLISHED";

        try (Statement stm = connection.createStatement())
        {

            try (ResultSet resultSet = stm.executeQuery(sql))
            {

                if (resultSet.next())
                {
                    return resultSet.getInt(1);
                }

            }

        }
        catch (SQLException e)
        {
            System.out.println(e.getNextException());
            e.printStackTrace();
        }

        return 0;


    }

    //delete first news
    public void deleteNews()
    {

        String sql = "DELETE FROM unpublished WHERE id = any(array(SELECT id FROM unpublished ORDER BY id LIMIT 1))";

        try (Statement stm = connection.createStatement())
        {

            stm.executeUpdate(sql);

        }

        catch (SQLException e)
        {
            System.out.println(e.getNextException());
            e.printStackTrace();
        }

    }

    //get all distinct news types of xml
    public String [] getNewsTypes() throws SQLException
    {

        String [] types = null;

        String sql = "SELECT xpath ('//type[not(preceding::type/. = .)]',news_xml) FROM published ORDER BY ID DESC LIMIT 1";


        try (Statement stm = connection.createStatement())
        {

            try (ResultSet resultSet = stm.executeQuery(sql))
            {

                if (resultSet.next())
                {

                    String str = resultSet.getString(1).replaceAll("<type>", "").replaceAll("</type>", "").replace("{", "").replace("}", "");   
                    types = str.split(",");

                }

            }

        }

        return types;

    }
    
    //get number of news with that type
    public int getCountByType(String type) throws SQLException
    {

        String sql = "SELECT xpath ('count(//news[type = \""+type+"\" ])',news_xml) FROM published ORDER BY ID DESC LIMIT 1";


        try (Statement stm = connection.createStatement())
        {

            try (ResultSet resultSet = stm.executeQuery(sql))
            {

                if (resultSet.next())
                {

                    String str = resultSet.getString(1).replace("{", "").replace("}", "");
                    return Integer.parseInt(str);

                }

            }

        }

        return 0;

    }

    //get news introducing a offset and limit
    public String getNewsByPage(int page) throws SQLException
    {

        int firstPosition = 6 * (page - 1) + 1;
        int lastPosition = firstPosition + 5;

        String sql = "SELECT xpath ('//news[position()>=" + firstPosition + " and position()<=" + lastPosition + "]',news_xml) FROM published ORDER BY ID DESC LIMIT 1";

        String newsXml = "";

        try (Statement stm = connection.createStatement())
        {

            try (ResultSet resultSet = stm.executeQuery(sql))
            {

                if (resultSet.next())
                {

                    newsXml = resultSet.getString(1);

                    if (newsXml.indexOf("\"") >= 0)
                    {
                        return newsXml.substring(newsXml.indexOf("\"") + 1, newsXml.lastIndexOf("\""));

                    }
                }

            }

        }

        return newsXml;

    }

    //get number of all news
    public int getCountNews() throws SQLException
    {

        String sql = "SELECT xpath ('count(//news)',news_xml) FROM published ORDER BY ID DESC LIMIT 1";


        try (Statement stm = connection.createStatement())
        {

            try (ResultSet resultSet = stm.executeQuery(sql))
            {

                if (resultSet.next())
                {

                    String str = resultSet.getString(1).replace("{", "").replace("}", "");
                    return Integer.parseInt(str);

                }

            }

        }

        return 0;

    }

    public void close()
    {
        try
        {
            this.connection.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}

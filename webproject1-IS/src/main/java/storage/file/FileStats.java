package storage.file;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import model.Stats;
import storage.pgsql.PgsqlStorageService;


public class FileStats
{
    
    private final String PATHSCRIPT = "WebContent/js/chart.js";
    private final String PATHCSV = "WebContent/data/stats.csv";

    public void writeFilesStats (PgsqlStorageService pgsql, Stats stats) throws FileNotFoundException, UnsupportedEncodingException, SQLException {

        writeScriptFirstChart(pgsql,stats);
        writeCsvData(pgsql);
        
    }
    
    private void writeScriptFirstChart (PgsqlStorageService pgsql, Stats stats) throws FileNotFoundException, UnsupportedEncodingException {
        
        PrintWriter writer = new PrintWriter(PATHSCRIPT, "UTF-8");
        List <Stats> statsList = pgsql.getStats();
        
        //create a c3js script to generate a chart with info about last 12 hours introduced news
        
        String output = "var chart = c3.generate({\n"
                + "bindto: '#chart',"
                + "data : {\n"
                + "columns : [\n";

        output+= "[ 'Number of News', ";
        
        String prefix = "";
        
        for (Stats s : statsList) {
        
            output += prefix + s.getNumber(); 
            prefix = ",";
        }
        
        output+= "]\n]\n"
        + "},\n"
        + "});";
        
        writer.println(output);
        
        writer.close();
        
    }
    
    private void writeCsvData (PgsqlStorageService pgsql) throws FileNotFoundException, UnsupportedEncodingException, SQLException {
        
        
        PrintWriter writer = new PrintWriter(PATHCSV, "UTF-8");
        
        //get distinc types present on XML
        String [] types = pgsql.getNewsTypes(); 
        
        //creates a csv with the format "name,count\ntype,x"
        if (types!=null) {
        
            String output = "name,count\n";
            
            int i=0;
            for (String t : types){
                
                i++;
                output+=t+","+pgsql.getCountByType(t);
                
                if (i!=types.length){
                    output+="\n";
                }
                
            }
            
            writer.print(output.trim());
            
            writer.close();
        
        }
        
    }
    
}

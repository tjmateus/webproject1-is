package storage.file;

import java.io.*;

import model.*;

public class FileNews {
    
    private final String PATH = "src/main/resources/storage/news.txt";
    
    //read allnews from a file
    public AllNews read() throws IOException {
        try 
        {
            AllNews news = new AllNews();
            FileInputStream fileInputStream = new FileInputStream(PATH);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream); 
            news = (AllNews) objectInputStream.readObject();
            objectInputStream.close();
            return news;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        } 
        catch (ClassNotFoundException e) 
        {
            e.printStackTrace();
        }
        return null;
    }

    //write all news to a file
    public void write(AllNews news) throws IOException {
        try
        {
            FileOutputStream fileOuputStream = new FileOutputStream(new File(PATH));
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOuputStream); 
            objectOutputStream.writeObject(news);
            objectOutputStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

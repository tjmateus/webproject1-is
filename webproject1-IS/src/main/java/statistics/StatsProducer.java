

package statistics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import model.Stats;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class StatsProducer
{

    private final String PATH;
    
    private Date startDate;
    private Date endDate;
    
    public StatsProducer (String clientId) {
        this.PATH = "src/main/resources/xml/"+clientId+".xml";
    }
    
    public Stats produceStats() throws NamingException, JMSException, IOException, ParserConfigurationException, SAXException, XPathExpressionException
    {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setValidating(false);

        DocumentBuilder db = dbf.newDocumentBuilder();
        
        Document doc = db.parse(new FileInputStream(new File(PATH)));

        XPathFactory factory = XPathFactory.newInstance();

        XPath xpath = factory.newXPath();
        
        String expression = "count(//news[translate(translate(translate(translate(translate(translate(date,'-',''),':',''),'.',''),'+',''),'T',''),'Z','') > "+getTimeLast12Hours()+"])";
        
        //count of news that have been introduced in last 12 hours, doing a xpath in xml
        double count = (double) xpath.evaluate(expression, doc, XPathConstants.NUMBER);
        
        return new Stats ((int) count, startDate, endDate);

    }
    
    //produce a string with time in last 12 hours
    private String getTimeLast12Hours()
    {

        this.endDate = new Date();
        
        long last12hours = endDate.getTime() - (60 * 60 * 1000 * 12);

        this.startDate = new Date(last12hours);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        String year = "" + cal.get(Calendar.YEAR);
        String month = "" + (cal.get(Calendar.MONTH) + 1);
        
        if (month.length()!=2){
            month = "0" + month;
        }
        
        String day = "" + cal.get(Calendar.DAY_OF_MONTH);
        
        if (day.length()!=2){
            day = "0" + day;
        }
        
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        String minute = "" + cal.get(Calendar.MINUTE);
        
        if (minute.length()!=2){
            minute = "0" + minute;
        }
        
        String second = "" + cal.get(Calendar.SECOND);
        
        if (second.length()!=2){
            second = "0" + second;
        }
        
        String hourStr = "" + hour;
        
        if (hourStr.length()!=2){
            hourStr = "0" + hourStr;
        }
        
        return year + month + day + hourStr + minute + second;
    }
    
    public String getPath () {
        return PATH;
    }
    
}

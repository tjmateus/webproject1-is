<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">




<html>

<head>

<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="description" content=""/>
<meta name="author" content=""/>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

<meta name="description" content=""/>
<meta name="author" content=""/>

<meta name="viewport" content="width=device-width,initial-scale=1"/>

<!-- CSS concatenated and minified via ant build script-->
<link rel="stylesheet" href="css/reset.css"/>
<link rel="stylesheet" href="css/style.css"/>
<!-- end CSS-->

<script src="js/libs/modernizr-2.0.6.min.js"></script>

<title>CNN</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet"/>
<link href="css/index.css" rel="stylesheet"/>
<!-- Custom CSS -->
<link href="css/agency.css" rel="stylesheet"/>

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css"/>
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css"/>
<link href='http://fonts.googleapis.com/css?family=Kaushan+Script'
	rel='stylesheet' type='text/css'/>
<link
	href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic'
	rel='stylesheet' type='text/css'/>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700'
	rel='stylesheet' type='text/css'/>

<!-- Load c3.css -->
<link href="css/c3.css" rel="stylesheet" type="text/css"/>

<!-- Load d3.js and c3.js -->
<script src="js/d3.min.js" charset="utf-8"></script>
<script src="js/c3.min.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index" onload="getPagination()">

	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand page-scroll" href="#page-top"> <img
				src="img/Cnn.jpg" alt="CNN" style="width: 129px; height: 40px"/>

			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class="hidden"><a href="#page-top"></a></li>
				<li><a class="page-scroll" href="#topnews">Top News</a></li>
				<li><a class="page-scroll" href="#allnews">All News</a></li>
				<li><a class="page-scroll" href="#statistics">Statistics</a></li>
				<li><a class="page-scroll" href="#about">About</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid --> </nav>

	<!-- Header -->
	<header>
	<div class="container">
		<div class="intro-text">
			<div class="intro-lead-in">Presenting News by</div>
			<div class="intro-heading">CNN</div>
		</div>
	</div>
	</header>

	<!-- Top News Section -->
	<section id="topnews">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Top News</h2>
			</div>
		</div>
		<!-- Page Content -->
		<div class="row carousel-holder">

			<div class="col-md-12">
				<div id="carousel-example-generic" class="carousel slide"
					data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0"
							class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						<li data-target="#carousel-example-generic" data-slide-to="3"></li>
						<li data-target="#carousel-example-generic" data-slide-to="4"></li>
					</ol>
					
    				
					<div class="carousel-inner">
					
						
    					<xsl:for-each select="//news">	
					
						<xsl:choose>
						
        				<xsl:when test="position()=1">
					
						<div class="item active">
							<xsl:choose>
   						 <xsl:when test="photo != ''">
    
					
						<img class="slide-image"
						alt="img">
						<xsl:attribute name="src">
        					<xsl:value-of select="photo"/>
    					</xsl:attribute>
    					
    					</img>
    					
    					</xsl:when>
    					<xsl:otherwise>
        					<img src="img/no-image.jpg" class="slide-image" alt="img"/>
    					</xsl:otherwise>
						</xsl:choose>
    						
    						<div class="carousel-caption">
    						
                    			<h2>
    								
    								<xsl:value-of select="title"/>
    								
                    			</h2>
                			</div>
    						
						</div>
						
         				</xsl:when>
    					
    					<xsl:otherwise>
    					
						<xsl:choose>
						
    					
         				<xsl:when test="position()&lt; 6">
         				
						<div class="item">
						
							<xsl:choose>
   						 	<xsl:when test="photo != ''">
    
								<img class="slide-image" alt="img">
							
							<xsl:attribute name="src">
        						<xsl:value-of select="photo"/>
    						</xsl:attribute>
    					
    						</img>
    					
    						</xsl:when>
    						<xsl:otherwise>
        						<img src="img/no-image.jpg" class="slide-no-image" alt="img"/>
    						</xsl:otherwise>
							</xsl:choose>
							
							<div class="carousel-caption">
    						
                    			<h2>
    								
                    				<xsl:value-of select="title"/>
                    				
                    			</h2>
                			</div>
							
						</div>
						
         				</xsl:when>
         				</xsl:choose>
         				
         				</xsl:otherwise>
       					</xsl:choose>
    					
						
						</xsl:for-each>
						
					</div>
					
					
					<a class="left carousel-control" href="#carousel-example-generic"
						data-slide="prev"> <span
						class="glyphicon glyphicon-chevron-left"></span>
					</a> <a class="right carousel-control" href="#carousel-example-generic"
						data-slide="next"> <span
						class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</div>
			</div>

		</div>
	</div>
	</section>

	<!-- All News Grid Section -->
	<section id="allnews" class="bg-light-gray">
	<div id="allNewsContainer" class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">All News</h2>
			</div>
		</div>
		<div id="dataDiv" class="row">
		
			<xsl:for-each select="//news">
			
			<xsl:choose>
			
			<xsl:when test="position()&lt; 7">
			
			<div class="col-md-4 col-sm-6 portfolio-item">
				<div class="div-title">
				<a class="portfolio-link"
					data-toggle="modal">
					
					<xsl:attribute name="href">
        				<xsl:value-of select="concat('#',@id)"/>
    				</xsl:attribute>
					
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-plus fa-3x"></i>
						</div>
					</div> 
					<div class="div-image">
					
					<xsl:choose>
   						 <xsl:when test="photo != ''">
    
					
						<img class="img-responsive"
						alt="img">
						<xsl:attribute name="src">
        					<xsl:value-of select="photo"/>
    					</xsl:attribute>
    					
    					</img>
    					
    					</xsl:when>
    					<xsl:otherwise>
        					<img src="img/no-image.jpg" class="img-responsive" alt="img"/>
    					</xsl:otherwise>
						</xsl:choose>
    				</div>
				</a>
				<div class="portfolio-caption">
					<h4><xsl:value-of select="title"/></h4>
					<p class="text-muted"><xsl:value-of select="date"/></p>
				</div>
				</div>
			</div>
			
			</xsl:when>
			
			</xsl:choose>
			
			</xsl:for-each>
			
		</div>
			
			<div id="center-pagination"></div>
			
	</div>


	</section>


	<!-- Statistics Section -->
	<section id="statistics" class="bg-light-gray">
	<div id="chart-container" class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Statistics</h2>
			</div>
		</div>
		
		<div class="caption">
			<h3>Evolution of Number of News Introduced</h3>
		</div>
		
		<div id="chart" class="chart"></div>

		<script src="js/chart.js" type="text/javascript"></script>

	</div>
	
	<div class="caption">
		<h3>Quantity of news by type</h3>
	</div>
	
	<div id="container" class="container">
    <div id="main" role="main">
      <div id="vis"></div>
    </div>
  	</div> <!--! end of #container -->
	
	
	</section>

	<!-- About Section -->
	<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">About</h2>
			</div>
		</div>
		<div class="row" align="center">
			<div class="about-text">
			
			<h3>IS Project 1 2014/2015 Developed By:</h3>
			
			</div>
			<div class="member1">
				<div class="col-md-5 col-sm-6">
					<div class="timeline-image">
						<img class="img-circle img-responsive" src="img/about/tiago.jpg"
							alt=""  width ="250px"/>
					</div>
					<div class="timeline-panel">
						<div class="timeline-heading" align="center">
							<h4>Tiago Mateus</h4>
							<h4 class="subheading">2010146711</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="member2">
				<div class="col-md-5 col-sm-6">
					<div class="timeline-image">
						<img class="img-circle img-responsive" src="img/about/micael.jpg"
							alt="" width ="250px"/>
					</div>
					<div class="timeline-panel" align="center">
						<div class="timeline-heading" align="center">
							<h4>Micael Reis</h4>
							<h4 class="subheading">2010143871</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>


	<footer>
	<div class="container">
		<div class="row">
			<div class="footer">
				<span class="copyright">Copyright Project #1 - IS 2014</span>
			</div>
		</div>
	</div>
	</footer>

	<!-- Portfolio Modals -->
	<!-- Use the modals below to showcase details about your portfolio projects! -->

	<!-- Portfolio Modal 1 -->
	
	<div id="all-modals">
	
    <xsl:for-each select="//news">
    
    <xsl:choose>
    
    <xsl:when test="position()&lt; 7">
	
	<div id="news-modals" class="portfolio-modal modal fade"
		tabindex="-1" role="dialog" aria-hidden="true">
		
		<xsl:attribute name="id">
        	<xsl:value-of select="@id"/>
    	</xsl:attribute>
		
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="modal-body">
							<!-- Project Details Go Here -->
							<h2><xsl:value-of select="title"/></h2>
							<p class="item-intro text-muted"><xsl:value-of select="url"/></p>
								
							<ul class="list-inline" text-align="center">
							
								<li><strong>Authors:</strong></li>
								<p></p>
								<xsl:for-each select="./authors/*">
									<li><xsl:value-of select="."/></li>
								</xsl:for-each>
								
							</ul>
							
							<ul class="list-inline" text-align="center">
							
								<li><strong>Highlights:</strong></li>
								<p></p>
								<xsl:for-each select="./highlights/*">
									<li><xsl:value-of select="."/></li>
								</xsl:for-each>
								
							</ul>
								
							
							<img class="img-responsive" alt="">
							
							<xsl:attribute name="src">
        						<xsl:value-of select="photo"/>
    						</xsl:attribute>
							
							</img>
							<p><xsl:value-of select="text"/></p>
							
							
							<ul class="list-inline">
							
								<li>Date: <xsl:value-of select="date"/></li>
								<li>Type: <xsl:value-of select="type"/></li>
								<li>Video: <xsl:value-of select="video"/></li>
								
							</ul>
							

							
							<button type="button" class="btn btn-primary"
								data-dismiss="modal">
								<i class="fa fa-times"></i> Close
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	</xsl:when>
	
	</xsl:choose>
	
	</xsl:for-each>
	
	</div>

	
	<!-- jQuery Version 1.11.0 -->
	<script src="js/jquery-1.11.0.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script
		src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="js/classie.js"></script>
	<script src="js/cbpAnimatedHeader.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>
	<script src="js/contact_me.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="js/agency.js"></script>

	<!-- Script to Activate the Carousel -->
	<script>
		$('.carousel').carousel({
			interval : 5000
		//changes the speed
		})
	</script>

	<script type="text/javascript">
		function onMoreNews(numP) {
			if (numP != 0) {
				$
						.get(
								'GetNewsByPage',
								{
									numPage : numP
								},
								function(responseText) {
									;
									var s = responseText.split("::!!::")
									document.getElementById("allNewsContainer").innerHTML = s[0];
									document.getElementById("all-modals").innerHTML = s[1];
								});
			}
		}
	</script>
	
	<script type="text/javascript">
		function getPagination() {
			$
					.get(
							'GetPagination',
								{
									
								},
								function(responseText) {
									;
									document.getElementById("center-pagination").innerHTML = responseText;
								});
			
		}
	</script>
	
	<script  src="js/plugins.js"></script>
  	<script src="js/libs/coffee-script.js"></script>
  	<script src="js/libs/d3.min.js"></script>
 	<script type="text/coffeescript" src="coffee/vis.coffee"></script>
	 <script type="text/javascript">
  	</script>


  <script> 
    window._gaq = [['_setAccount','UA-17355070-1'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
      load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  </script>

</body>

</html>



	</xsl:template>
</xsl:stylesheet>